class Rectangle {
    constructor (width, length) {
        this.width = width;
        this.length = length;
    }

    getArea() {
        return this.width * this.length;
    }
}

let name ="Nguyen Van Nam";
let address = "Ha Noi, Viet Nam";
export {name, address}

export default Rectangle;