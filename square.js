import Rectangle from "./rectangle.js";

class Square extends Rectangle {
    constructor(width) {
        super(width, width);
        this.perimeter = this.width * 4;
    }

    getPerimeter() {
        return this.perimeter;
    }
}

export default Square;