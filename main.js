import Rectangle from "./rectangle.js";
import {name, address}  from "./rectangle.js";
import Square from "./square.js";


console.log(name);
console.log(address);

var rec1 = new Rectangle(10, 15);
console.log("width = " + rec1.width);
console.log("length = " + rec1.length);
console.log("getArea = " + rec1.getArea());

var rec2 = new Rectangle(100, 25);
console.log("width = " + rec2.width);
console.log("length = " + rec2.length);
console.log("getArea = " + rec2.getArea());  

var square1 = new Square(50,50);
console.log("width = " + square1.width);
console.log("length = " + square1.length);
console.log("getArea = " + square1.getArea());  

var square2 = new Square(25);
console.log("width = " + square2.width);
console.log("length = " + square2.length);
console.log("perimeter = " + square2.perimeter);
console.log("getArea = " + square2.getArea());  
console.log("getPerimeter = " + square2.getPerimeter());

console.log(rec1 instanceof Rectangle);
console.log(rec2 instanceof Rectangle);
console.log(rec2 instanceof Square);
console.log(square1 instanceof Rectangle);
console.log(square2 instanceof Square);